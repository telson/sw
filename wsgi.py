import random
from flask import Flask, render_template

application = Flask(__name__)

words = []


def random_line(lines):
    while True:
        word = random.choice(lines)
        if not any(word.strip() in s for s in words) and word.strip() != '':
            return word.strip()


@application.route("/")
def swear():
    words = []
    while len(words) < 3:
        words.append(random_line(open('bad-words.txt').read().splitlines()))

    insult = 'You ' + ' '.join(map(str, words)) + '!'

    return render_template('index.html', swear=insult)


if __name__ == "__main__":
    application.run()
